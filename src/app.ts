import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import compression from 'compression';
import {v1Router, interval} from './api/v1';
import {MultipagoSocket} from './api/model/socket';
import {SocketOptions} from './api/model/socketOptions';
import net, {ServerOpts} from 'net';
import {ProcessType} from './api/model/processType';


import {
  flujoAnulacion,
  flujoChip,
  flujoCierre,
  flujoCtl,
  flujoInicio,
} from './api/Library/flowPOS';
const libPOS = require('./api/Library/libPOS');

const origin = {
  origin: '*',
};

//-----------------------------------------------

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors(origin));
app.use(compression());
app.use(helmet());
app.use(morgan('combined'));
app.use('/api/v1', v1Router);

app.locals.socketList = {};

const port = process.env.PORT || 5001;

app.listen(port, () => {
  console.log(`[App]: Listening on port ${port}`);
});
//------------------------------------------
const tcpPort: number = 5432;

var options: ServerOpts = {
  allowHalfOpen: true,
  pauseOnConnect: false,
};

var server = net.createServer(options);

server.on('connection', function (socket: net.Socket) {
  var rport = socket.remotePort;
  var raddr = socket.remoteAddress;
  libPOS.log('INFO', 'Servidor TCP-IP conectado a: ' + raddr + ':' + rport);
  var destroyed = socket.destroyed;
  socket.setEncoding(null);

  let mpSocket: MultipagoSocket = {
    options: {
      confirmation: 0,
      isAck1: false,
      isAck2: false,
      isAck3: false,
      isAck4: false,
      montoBOB: 0.0,
      paso: 0,
      reciboTRA: '',
      respHost: '',
      respHostJson: '',
      tam: 0,
      type: ProcessType.CHIP,
    },
    remoteAddress: raddr,
    remotePort: rport,
    socket: socket,
  };
  app.locals.socketList['::ffff:'] = mpSocket;

  // The server can also receive data from the client by reading from its socket.
  socket.on('data', function (chunk) {
    var resp = Buffer.from(chunk);
    var strReply = resp.toString('hex');
    var raddr = socket.remoteAddress;
    let sock2: MultipagoSocket = app.locals.socketList['::ffff:'];
    console.log('buffer string:', strReply);
    libPOS.log('INFO', `POS: ${resp.toString('hex')} (${resp.length})`);
    sock2.options.tam += resp.length;
    //LibPos.log('INFO',`Tam:  ${tam}`);

    switch (sock2.options.type) {
      case ProcessType.CHIP:
        flujoChip(strReply, sock2);
        break;
      case ProcessType.CTL:
        flujoCtl(strReply, sock2);
        break;
      case ProcessType.Inicio:
        flujoInicio(strReply, sock2);
        break;
      case ProcessType.Cierre:
        flujoCierre(strReply, sock2);
        break;
      case ProcessType.Anulacion:
        flujoAnulacion(strReply, sock2);
        break;
      default:
        break;
    }
  });

  // When the client requests to end the TCP connection with the server, the server
  // ends the connection.
  socket.on('close', function (data) {
    var islistening = server.listening;
    var destroyed = socket.destroyed;
    libPOS.log('WARN', 'Socket Close');
  });

  // Don't forget to catch error, for your own sake.
  socket.on('error', function (err) {
    var islistening = server.listening;
    var destroyed = socket.destroyed;
    libPOS.log('ERROR', 'Socket Error');
    //LibPos.log('ERROR','-- Servidor TCP escuchando: ' + islistening);
    //LibPos.log('ERROR','-- Socket destroyed: ' + destroyed);
    libPOS.log('ERROR', `-- Error: ${err.toString()}`);
    libPOS.log('ERROR', err.stack);
  });

  socket.on('end', function (data) {
    var islistening = server.listening;
    var destroyed = socket.destroyed;
    //clearInterval(interval);
    libPOS.log('WARN', 'Socket End');
  });
});

server.listen(tcpPort, function () {
  //'listening' listener
  libPOS.log('INFO', 'Servidor escuchando en el puerto: ' + tcpPort);
});

// emits when any error occurs -> calls closed event immediately after this.
server.on('error', function (error) {
  libPOS.log('ERROR', 'Server Error');
  libPOS.log('ERROR', '-- Error: ' + error.toString());
});
