import {Router} from 'express';
import {posRouter, interval} from './routes';

const v1Router = Router();

v1Router.get('/', (req, res) => {
  console.log('ingreso!!!!!!')
  return res.json({message: 'This API is up and running 🎉'});
});

v1Router.use('/POS', posRouter);

export {v1Router, interval};
