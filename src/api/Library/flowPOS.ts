import {MultipagoSocket} from '../model/socket';
import {POS} from './enumPOS';
var libPOS = require('./libPOS');

function flujoChip(strReply: string, socket: MultipagoSocket) {
  var resp = '';
  switch (socket.options.paso) {
    case 1:
      if (socket.options.isAck1 && socket.options.tam == 40) {
        // Ultima Transaccion
        //socket.write(ackHex); console.log(`Enviado al POS: ${ack}`);
        resp = libPOS.SendAck(socket.socket); //console.log(`CAJA: ACK: ${resp}`);
        resp = libPOS.SendTransRevNo(socket.socket); //console.log(`CAJA: Enviado revNO al POS: ${resp}`);
        //tam = 0; console.log(`Tamaño: ${tam}`);
        socket.options.paso = 2; //console.log(`Siguiente paso: ${paso}`);
        socket.options.tam = 0;
        break;
      } else if (isAck(strReply)) {
        socket.options.isAck1 = true;
        socket.options.tam = 0;
        break;
      }

    case 2:
      if (socket.options.isAck2 && socket.options.tam == 36) {
        // Solicitud nueva pantalla
        resp = libPOS.SendAck(socket.socket); //console.log(`CAJA: ACK: ${resp}`);
        //tam = 0; console.log(`Tamaño: ${tam}`);
        socket.options.paso = 3; //console.log(`Siguiente paso: ${paso}`);
        socket.options.tam = 0;
        break;
      } else if (isAck(strReply)) {
        socket.options.isAck2 = true;
        socket.options.tam = 0;

        break;
      }
    case 3:
      if (socket.options.tam == 29) {
        // Solicitud de datos
        resp = libPOS.SendAck(socket.socket); //console.log(`CAJA: ACK: ${resp}`);
        var tramaf = libPOS.SendDataToPos(
          socket.options.montoBOB,
          socket.socket
        ); //console.log(`CAJA: Datos al POS: ${tramaf}`);  // Envio de datos
        //console.log('Trama:' + tramaf);
        //socket.write(envDatosHex); console.log(`CAJA: Datos al POS: ${envDatos}`);
        //tam = 0; console.log(`Tamaño: ${tam}`);
        socket.options.paso = 4; //console.log(`Siguiente paso: ${paso}`);
        socket.options.tam = 0;
        break;
      } else {
        //en caso no ingrese su tarjeta el cliente ingresara aqui
        socket.options.respHost = strReply;
        if(socket.options.respHost === '15'){  //NACK EL POS CIERRA LA TRANSACCION
          socket.options.respHostObject = {"error":"Ha ocurrido un problema, vuelva a intentar"};
          socket.options.respHostJson = `{"error":"Ha ocurrido un problema, vuelva a intentar"}`;    
        }
        break;
      }

    case 4:
      if (socket.options.isAck4 && socket.options.tam == 36) {
        // Solicitud pantalla PIN
        resp = libPOS.SendAck(socket.socket);
        console.log(`CAJA: ACK: ${resp}`);
        //tam = 0; console.log(`Tamaño: ${tam}`);
        socket.options.paso = 5; //console.log(`Siguiente paso: ${paso}`);
        socket.options.tam = 0;
        break;
      } else if (isAck(strReply)) {
        socket.options.isAck4 = true;
        socket.options.tam = 0;

        break;
      }

    case 5:
      if (socket.options.tam >= 223) {
        // Respuesta del Host
        resp = libPOS.SendAck(socket.socket); //console.log(`CAJA: ACK: ${resp}`);
        socket.options.respHost = socket.options.respHost + strReply;
        socket.options.respHostJson = libPOS.RespuestaHostVenta(
          socket.options.respHost
        );
        socket.options.respHostObject = JSON.parse(socket.options.respHostJson);
        socket.options.paso = -1;
        socket.options.tam = 0;
        socket.options.isAck1 = false;
        socket.options.isAck2 = false;
        socket.options.isAck3 = false;
        socket.options.isAck4 = false;
        break;
      } else {
        socket.options.respHost = strReply;
        if(socket.options.respHost === '15'){  //NACK EL POS CIERRA LA TRANSACCION
          socket.options.respHostObject = {"error":"Ha ocurrido un problema, vuelva a intentar"};
          socket.options.respHostJson = `{"error":"Ha ocurrido un problema, vuelva a intentar"}`;    
        }
        break;
      }
    default:
      break;
  }
}

function flujoCtl(strReply: string, socket: MultipagoSocket) {
  var resp = '';
  switch (socket.options.paso) {
    case 1:
      if (socket.options.isAck1 && socket.options.tam == 40) {
        // Ultima Transaccion
        //socket.write(ackHex); console.log(`Enviado al POS: ${ack}`);
        resp = libPOS.SendAck(socket.socket); //console.log(`CAJA: ACK: ${resp}`);
        resp = libPOS.SendTransRevNo(socket.socket); //console.log(`CAJA: Enviado revNO al POS: ${resp}`);
        //tam = 0; console.log(`Tamaño: ${tam}`);
        socket.options.paso = 2; //console.log(`Siguiente paso: ${paso}`);
        socket.options.tam = 0;
        break;
      } else if (isAck(strReply)) {
        socket.options.isAck1 = true;
        socket.options.tam = 0;
        break;
      }

    case 2:
      if (socket.options.isAck2 && socket.options.tam == 29) {
        // Solicitud de datos
        resp = libPOS.SendAck(socket.socket); //console.log(`CAJA: ACK: ${resp}`);
        //socket.write(envDatosHex);
        var tramaf = libPOS.SendDataToPos(
          socket.options.montoBOB,
          socket.socket
        ); //console.log(`CAJA: Datos al POS: ${tramaf}`);  // Envio de datos
        //tam = 0; console.log(`Tamaño: ${tam}`);
        socket.options.paso = 3; //console.log(`Siguiente paso: ${paso}`);
        socket.options.tam = 0;
        break;
      } else if (isAck(strReply)) {
        socket.options.isAck2 = true;
        socket.options.tam = 0;

        break;
      }

    case 3:
      if (socket.options.isAck3 && socket.options.tam == 36) {
        // Solicitud pantalla acerque tarjeta
        resp = libPOS.SendAck(socket.socket); //console.log(`CAJA: ACK: ${resp}`);
        resp = libPOS.SendTipoTarjetaCtl(socket.socket); //console.log(`CAJA: Lectura CTL: ${resp}`);  // Envio de tipo tarjeta CTL
        //tam = 0; console.log(`Tamaño: ${tam}`);
        socket.options.paso = 4; //console.log(`Siguiente paso: ${paso}`);
        socket.options.tam = 0;
        break;
      } else if (isAck(strReply)) {
        socket.options.isAck3 = true;
        socket.options.tam = 0;
        break;
      }

    case 4:
      if (socket.options.isAck4 && socket.options.tam == 36) {
        // Solicitud pantalla PIN
        resp = libPOS.SendAck(socket.socket); //console.log(`CAJA: ACK: ${resp}`);
        //tam = 0; console.log(`Tamaño: ${tam}`);
        socket.options.paso = 5; //console.log(`Siguiente paso: ${paso}`);
        socket.options.tam = 0;
        break;
      } else if (isAck(strReply)) {
        socket.options.isAck4 = true;
        socket.options.tam = 0;

        break;
      }

    case 5:
      if (socket.options.tam >= 222) {
        // Respuesta del Host
        resp = libPOS.SendAck(socket.socket); //console.log(`CAJA: ACK: ${resp}`);
        socket.options.respHost = socket.options.respHost + strReply; //console.log('RespHost:' + respHost);
        socket.options.respHostJson = libPOS.RespuestaHostVenta(
          socket.options.respHost
        );
        socket.options.respHostObject = JSON.parse(socket.options.respHostJson);
        socket.options.paso = -1;
        socket.options.tam = 0;
        socket.options.isAck1 = false;
        socket.options.isAck2 = false;
        socket.options.isAck3 = false;
        socket.options.isAck4 = false;
        socket.options.respHost = '';
        break;
      } else {
        socket.options.respHost = strReply;
        if(socket.options.respHost === '15'){  //NACK EL POS CIERRA LA TRANSACCION
          socket.options.respHostObject = {"error":"Ha ocurrido un problema, vuelva a intentar"};
          socket.options.respHostJson = `{"error":"Ha ocurrido un problema, vuelva a intentar"}`;          
        }
        break;
      }
    default:
      break;
    //socket.write(ackHex); console.log(`Enviado al POS: ${ack}`);
  }
}

function flujoCierre(strReply: string, socket: MultipagoSocket) {
  var resp = '';
  switch (socket.options.paso) {
    case 1:
      if (socket.options.isAck1 && socket.options.tam == 38) {
        // Cantidad de transacciones
        //socket.write(ackHex); console.log(`Enviado al POS: ${ack}`);
        resp = libPOS.SendAck(socket.socket); //console.log(`CAJA: ACK: ${resp}`);
        socket.options.respHost += strReply;
        var codr = socket.options.respHost.substr(50, 4); //console.log('Cod.Respuesta:' + codr);
        if (codr == '5858') {
          socket.options.paso = -1;
          socket.options.isAck1 = false;
          socket.options.isAck2 = false;
          socket.options.respHostJson = libPOS.RespuestaHostCierreSinTransac(
            socket.options.respHost
          );
        } else {
          socket.options.paso = 2; //console.log(`Siguiente paso: ${paso}`);
          socket.options.tam = 0;
        }
        socket.options.respHost = '';
        //tam = 0; console.log(`Tamaño: ${tam}`);

        break;
      } else if (isAck(strReply)) {
        socket.options.isAck1 = true;
        socket.options.tam = 0;
        break;
      } else {
        socket.options.respHost = strReply;
        break;
      }

    case 2:
      if (socket.options.tam > 140) {
        // Mas transacciones
        resp = libPOS.SendAck(socket.socket); //console.log(`CAJA: ACK: ${resp}`);
        socket.options.tam = 0;
        break;
      } else if (socket.options.tam == 40) {
        // Fin Cierre
        socket.options.respHost += strReply;
        resp = libPOS.SendAck(socket.socket); //console.log(`CAJA: ACK: ${resp}`);
        socket.options.respHostJson = libPOS.RespuestaHostCierreConTransac(
          socket.options.respHost
        );
        socket.options.respHostObject = JSON.parse(socket.options.respHostJson);
        socket.options.tam = 0;
        socket.options.paso = -1;
        socket.options.isAck1 = false;
        socket.options.isAck2 = false;
        socket.options.respHost = '';
        break;
      } else {
        socket.options.respHost = strReply;
        if(socket.options.respHost === '15'){  //NACK EL POS CIERRA LA TRANSACCION
          socket.options.respHostObject = {"error":"Ha ocurrido un problema, vuelva a intentar"};
          socket.options.respHostJson = `{"error":"Ha ocurrido un problema, vuelva a intentar"}`;    
        }
        break;
      }

    default:
      break;
    //socket.write(ackHex); console.log(`Enviado al POS: ${ack}`);
  }
}

function flujoInicio(strReply: string, socket: MultipagoSocket) {
  var resp = '';
  switch (socket.options.paso) {
    case 1:
      if (socket.options.isAck1 && socket.options.tam == 29) {
        // Respuesta Host
        //socket.write(ackHex); console.log(`Enviado al POS: ${ack}`);
        resp = libPOS.SendAck(socket); //console.log(`CAJA: ACK: ${resp}`);
        socket.options.respHost += strReply;
        socket.options.respHostJson = libPOS.RespuestaHostInicializacion(
          socket.options.respHost
        );
        socket.options.tam = 0;
        socket.options.paso = -1;
        socket.options.isAck1 = false;
        socket.options.isAck2 = false;
        socket.options.respHost = '';

        //tam = 0; console.log(`Tamaño: ${tam}`);

        break;
      } else if (isAck(strReply)) {
        socket.options.isAck1 = true;
        socket.options.tam = 0;
        break;
      } else {
        socket.options.respHost = strReply;
        break;
      }

    default:
      break;
    //socket.write(ackHex); console.log(`Enviado al POS: ${ack}`);
  }
}

function flujoAnulacion(strReply: string, sock2: MultipagoSocket) {
  var resp = '';
  switch (sock2.options.paso) {
    case 1:
      if (sock2.options.isAck1 && sock2.options.tam == 29) {
        // Solicitud de referencia
        //socket.write(ackHex); console.log(`Enviado al POS: ${ack}`);
        resp = libPOS.SendAck(sock2.socket); //console.log(`CAJA: ACK: ${resp}`);
        resp = libPOS.SendRefAnulToPos(sock2.options.reciboTRA, sock2.socket); //console.log(`CAJA: Referencia anulacion: ${resp}`); // Envio de referencia
        //tam = 0; console.log(`Tamaño: ${tam}`);
        sock2.options.paso = 2; //console.log(`Siguiente paso: ${paso}`);
        sock2.options.tam = 0;
        break;
      } else if (isAck(strReply)) {
        sock2.options.isAck1 = true;
        sock2.options.tam = 0;
        break;
      }

    case 2:
      if (sock2.options.isAck2 && sock2.options.tam == 29) {
        // Resultado busqueda referencia
        resp = libPOS.SendAck(sock2.socket); //console.log(`CAJA: ACK: ${resp}`);
        //socket.write(envDatosHex);
        resp = libPOS.SendConfirmarAnulacion(sock2.socket); //console.log(`CAJA: Confirmar anulacion: ${resp}`);  // Confirmar anulacion
        //tam = 0; console.log(`Tamaño: ${tam}`);
        sock2.options.paso = 3; //console.log(`Siguiente paso: ${paso}`);
        sock2.options.tam = 0;
        break;
      } else if (isAck(strReply)) {
        sock2.options.isAck2 = true;
        sock2.options.tam = 0;

        break;
      }

    case 3:
      if (sock2.options.tam >= 210) {
        // Respuesta del Host
        resp = libPOS.SendAck(sock2.socket); //onsole.log(`CAJA: ACK: ${resp}`);
        sock2.options.respHost = sock2.options.respHost + strReply;
        sock2.options.respHostJson = libPOS.RespuestaHostAnulacion(
          sock2.options.respHost
        );
        sock2.options.paso = -1;
        sock2.options.tam = 0;
        sock2.options.isAck1 = false;
        sock2.options.isAck2 = false;
        sock2.options.isAck3 = false;
        sock2.options.isAck4 = false;
        sock2.options.respHost = '';
        break;
      } else {
        sock2.options.respHost = strReply;
        if(sock2.options.respHost === '15'){  //NACK EL POS CIERRA LA TRANSACCION
          sock2.options.respHostObject = {"error":"Ha ocurrido un problema, vuelva a intentar"};
          sock2.options.respHostJson = `{"error":"Ha ocurrido un problema, vuelva a intentar"}`;          
        }
        break;
      }
    default:
      break;
    //socket.write(ackHex); console.log(`Enviado al POS: ${ack}`);
  }
}
function isAck(str) {
  if (str == POS.ACK) {
    return true;
  } else {
    return false;
  }
}
export {flujoChip, flujoCtl, flujoCierre, flujoInicio, flujoAnulacion};
