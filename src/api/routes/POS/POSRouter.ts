import e, {Router} from 'express';
import { FailureMsgResponse, InternalErrorResponse, SuccessResponse } from '../../../utils/apiResponse';
import asyncHandler from '../../../utils/asyncHandler';
import {ProcessType} from '../../model/processType';
import {MultipagoSocket} from '../../model/socket';
var libPOS = require('../../Library/libPOS');
var intr;
var posRouter = Router();

posRouter.get(
  '/chip/:ammount',
  asyncHandler(async (req, res) => {
    try{
      var posSocket: MultipagoSocket = req.app.locals.socketList['::ffff:'];
      posSocket.options.type = ProcessType.CHIP;
      posSocket.options.respHostJson = '';
  
      var ammount: number = parseFloat(req.params.ammount);
      console.log('MONTO TRANSACCION:', ammount);
      libPOS.log('INFO', 'Pago con tarjeta Chip:' + ammount);
  
      if (isNaN(ammount)) {
        res.send('El monto debe ser un numero entero mayor a cero');
      } else {
        let ammountString = ammount.toFixed(2);
        posSocket.options.montoBOB = libPOS.ValidarMonto(ammountString);
        posSocket.options.paso = 1;
        posSocket.options.tam = 0;
        //sock2.write(solConnHex); console.log(`CAJA: Enviado solConn al POS: ${solConn}`);
        libPOS.SendConnectionChip(posSocket.socket);
        intr = setInterval(function () {
          var lenr = posSocket.options.respHostJson.length;
          if (lenr > 1) {
            libPOS.log('INFO', 'Resp:' + posSocket.options.respHostJson);
            if('error' in posSocket.options.respHostObject){
              new FailureMsgResponse('Ha ocurrido un error con la transacción, intente nuevamente').send(res);
            }else{
              new SuccessResponse('Transaccion realizada correctamente', posSocket.options.respHostObject).send(res);
            }
            clearInterval(intr);
            posSocket.options.respHost = '';
          }
        }, 3000);
      }
    }catch(e){
      console.log('Error', e);
     new FailureMsgResponse('Error, revise que el dispositivo se encuentra conectado').send(res);
    }    
  })
);

posRouter.get(
  '/ctl/:ammount/',
  asyncHandler(async (req, res) => {
    try{
      //res.send(req.params)
      var posSocket: MultipagoSocket =
      req.app.locals.socketList['::ffff:'];
      posSocket.options.type = ProcessType.CTL;
      posSocket.options.respHostJson = '';
      var ammount: number = parseFloat(req.params.ammount);
      console.log('MONTO TRANSACCION:', ammount);
      libPOS.log('INFO', 'Pago con tarjeta CTL:' + ammount);

      if (isNaN(ammount)) {
        res.send('El monto debe ser un numero entero mayor a cero');
      } else {
        let ammountString = ammount.toFixed(2);
        posSocket.options.montoBOB = libPOS.ValidarMonto(ammountString);
        posSocket.options.paso = 1;
        posSocket.options.tam = 0;
        //sock2.write(solConnHex); console.log(`CAJA: Enviado solConn al POS: ${solConn}`);
        libPOS.SendConnectionCtl(posSocket.socket);
        intr = setInterval(function () {
          var lenr = posSocket.options.respHostJson.length;
          if (lenr > 1) {
            libPOS.log('INFO', 'Resp:' + posSocket.options.respHostJson);
            if('error' in posSocket.options.respHostObject){
              new FailureMsgResponse('Ha ocurrido un error con la transacción, intente nuevamente').send(res);
            }else{
              new SuccessResponse('Transaccion realizada correctamente', posSocket.options.respHostObject).send(res);
            }
            clearInterval(intr);
            posSocket.options.respHost = '';
          }
        }, 3000);
      }
    }catch(e){
      console.log('Error', e);
      new FailureMsgResponse('Error, revise que el dispositivo se encuentra conectado').send(res);
    }    
  })
);

posRouter.get(
  '/inicializar/:confirmar',
  asyncHandler(async (req, res) => {
    var posSocket: MultipagoSocket =
      req.app.locals.socketList['::ffff:'];
    posSocket.options.type = ProcessType.Inicio;
    posSocket.options.confirmation = parseInt(req.params.confirmar);
    posSocket.options.respHostJson = '';

    libPOS.log('INFO', 'Inicializacion de Pos');
    //res.send('Pago con tarjeta CTL:' + req.params.monto);
    //console.log('-- Socket: ' + rport);
    if (posSocket.options.confirmation != 1) {
      res.send('Inicializacion no autorizada');
    } else {
      //res.send('Monto:' + montoBOB);
      posSocket.options.paso = 1;
      posSocket.options.tam = 0;
      //sock2.write(solConnHex); console.log(`CAJA: Enviado solConn al POS: ${solConn}`);
      libPOS.SendSolicitudInicializar(posSocket.socket);
      intr = setInterval(function () {
        var lenr = posSocket.options.respHostJson.length;
        if (lenr > 1) {
          libPOS.log('INFO', 'Resp:' + posSocket.options.respHostJson);
          res.send(posSocket.options.respHostJson);
          clearInterval(intr);
          posSocket.options.respHostJson = '';
        }
      }, 3000);
    }
  })
);

posRouter.get(
  '/cierre/:confirmar',
  asyncHandler(async (req, res) => {
    try{
      //res.send(req.params)
      var posSocket: MultipagoSocket =
      req.app.locals.socketList['::ffff:'];
      posSocket.options.type = ProcessType.Cierre;
      posSocket.options.confirmation = parseInt(req.params.confirmar);
      posSocket.options.respHostJson = '';

      libPOS.log('INFO', 'Cierre de lote');
      //res.send('Pago con tarjeta CTL:' + req.params.monto);
      //console.log('-- Socket: ' + rport);
      if (posSocket.options.confirmation != 1) {
      res.send('Cierre no autorizado');
      } else {
      //res.send('Monto:' + montoBOB);
      posSocket.options.paso = 1;
      posSocket.options.tam = 0;
      //sock2.write(solConnHex); console.log(`CAJA: Enviado solConn al POS: ${solConn}`);
      libPOS.SendSolicitudCierre(posSocket.socket);
      intr = setInterval(function () {
        var lenr = posSocket.options.respHostJson.length;
        if (lenr > 1) {
          libPOS.log('INFO', 'Resp:' + posSocket.options.respHostJson);
          if('error' in posSocket.options.respHostObject){
            new FailureMsgResponse('Ha ocurrido un error con el cierre de cajas, intente nuevamente').send(res);
          }else{
            new SuccessResponse('Cierre de cajas realizado correctamente', posSocket.options.respHostObject).send(res);
          }
          clearInterval(intr);
          posSocket.options.respHostJson = '';
          //libPOS = '';
        }
      }, 3000);
      }
    }catch(e){
      console.log('Error', e);
      new FailureMsgResponse('Error, revise que el dispositivo se encuentra conectado').send(res);
    }
    
  })
);

posRouter.get(
  '/anulacion/:recibo',
  asyncHandler(async (req, res) => {
    try{
      var posSocket: MultipagoSocket =
      req.app.locals.socketList['::ffff:'];
      posSocket.options.type = ProcessType.Anulacion;
      posSocket.options.confirmation = parseInt(req.params.confirmar);
      posSocket.options.respHostJson = '';

      var recibo = parseInt(req.params.recibo);

      libPOS.log('INFO', 'Anulacion de transaccion:' + recibo);

      if (isNaN(recibo)) {
        res.send('El monto debe ser un numero entero mayor a cero');
      } else {
        posSocket.options.reciboTRA = libPOS.ValidarRef(recibo);
        //res.send('Monto:' + montoBOB);
        posSocket.options.paso = 1;
        posSocket.options.tam = 0;
        //sock2.write(solConnHex); console.log(`CAJA: Enviado solConn al POS: ${solConn}`);
        libPOS.SendSolicitudAnulacion(posSocket.socket);
        intr = setInterval(function () {
          var lenr = posSocket.options.respHostJson.length;
          if (lenr > 1) {
            if('error' in posSocket.options.respHostObject){
              new FailureMsgResponse('Ha ocurrido un error con el cierre de cajas, intente nuevamente').send(res);
            }else{
              libPOS.log('INFO', 'Resp:' + posSocket.options.respHostJson);
              new SuccessResponse('Cierre de cajas realizado correctamente', posSocket.options.respHostObject).send(res);
            }        
            clearInterval(intr);
            posSocket.options.respHost = '';
          }
        }, 3000);
    }
    }catch(e){
      console.log('Error', e);
      new FailureMsgResponse('Error, revise que el dispositivo se encuentra conectado').send(res);
    }    
  })
);
export { intr as interval}
export default posRouter;
