import {Socket} from 'net';
import {SocketOptions} from './socketOptions';

export interface MultipagoSocket {
  remoteAddress: string;
  remotePort: number;
  options: SocketOptions;
  socket: Socket;
}
