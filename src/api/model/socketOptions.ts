import {ProcessType} from './processType';

export interface SocketOptions {
  confirmation: number;
  montoBOB: number;
  reciboTRA: string;
  respHost: string;
  respHostJson: string;
  respHostObject?: any;
  paso: number;
  tam: number;
  type: ProcessType;
  isAck1: Boolean;
  isAck2: Boolean;
  isAck3: Boolean;
  isAck4: Boolean;
}
