import {MultipagoSocket} from '../api/model/socket';

declare namespace Express {
  export interface locals {
    socketsAvailable: MultipagoSocket;
  }
}
