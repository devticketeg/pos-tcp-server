
# Servidor tcp para pagos con POS

Este proyecto contiene el codigo para levantar un servidor para conectarse a los POS y asi poder realizar cobros en caja.



## API

Se ha añadido un POSTMAN en la carpeta test

#### Inicio de operaciones

```http
  GET /api/v1/pos/inicializar/1
```

#### Cierre de operaciones

```http
  GET /api/v1/pos/cierre/1
```

#### Pago con chip

Para este tipo de pagos es necesario insertar la tarjeta en el POS

```http
  GET /api/v1/pos/chip/:monto
```

| Parametro | Tipo     | Descripcion                       |
| :-------- | :------- | :-------------------------------- |
| `monto`      | `string` | **Requerido**. Monto que debera cobrar el POS |

#### Pago contactless

Para este tipo de pagos solo es necesario acercar la tarjeta al POS

```http
  GET /api/v1/pos/ctl/:monto
```

| Parametro | Tipo     | Descripcion                       |
| :-------- | :------- | :-------------------------------- |
| `monto`      | `string` | **Requerido**. Monto que debera cobrar el POS |

#### Anulacion de pagos

```http
  GET /api/v1/pos/anulacion/:nrorecibo
```

| Parametro | Tipo     | Descripcion                       |
| :-------- | :------- | :-------------------------------- |
| `nrorecibo`      | `string` | **Requerido**. Nro del recibo que se imprimio en el POS cuando se realizo la transaccion |


## Levantar el proyecto

Para poder levantar el proyecto por primera vez es necesario tener instalado [NODEJS](!https://nodejs.org/es/) (version = 14.19
recomendamos la instalacion de [nvm](!https://github.com/coreybutler/nvm-windows) para poder trabajar con diferentes versiones de node)

Una vez instalado nodejs, clonamos el proyecto desde nuestra terminal

```bash
  git clone git@bitbucket.org:devticketeg/pos-tcp-server.git
```

Vamos al directorio del proyecto

```bash
  cd pos-tcp-server
```

Instalamos las dependecias

```bash
  npm install
```

E inicializamos el servidor con el comando

```bash
  npm run start:dev
```

