"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const morgan_1 = __importDefault(require("morgan"));
const cors_1 = __importDefault(require("cors"));
const body_parser_1 = __importDefault(require("body-parser"));
const helmet_1 = __importDefault(require("helmet"));
const compression_1 = __importDefault(require("compression"));
const v1_1 = require("./api/v1");
const net_1 = __importDefault(require("net"));
const processType_1 = require("./api/model/processType");
const flowPOS_1 = require("./api/Library/flowPOS");
const libPOS = require('./api/Library/libPOS');
const origin = {
    origin: '*',
};
//-----------------------------------------------
const app = (0, express_1.default)();
app.use(body_parser_1.default.json());
app.use(body_parser_1.default.urlencoded({ extended: true }));
app.use((0, cors_1.default)(origin));
app.use((0, compression_1.default)());
app.use((0, helmet_1.default)());
app.use((0, morgan_1.default)('combined'));
app.use('/api/v1', v1_1.v1Router);
app.locals.socketList = {};
const port = process.env.PORT || 5001;
app.listen(port, () => {
    console.log(`[App]: Listening on port ${port}`);
});
//------------------------------------------
const tcpPort = 5432;
var options = {
    allowHalfOpen: true,
    pauseOnConnect: false,
};
var server = net_1.default.createServer(options);
server.on('connection', function (socket) {
    var rport = socket.remotePort;
    var raddr = socket.remoteAddress;
    libPOS.log('INFO', 'Servidor TCP-IP conectado a: ' + raddr + ':' + rport);
    var destroyed = socket.destroyed;
    socket.setEncoding(null);
    let mpSocket = {
        options: {
            confirmation: 0,
            isAck1: false,
            isAck2: false,
            isAck3: false,
            isAck4: false,
            montoBOB: 0.0,
            paso: 0,
            reciboTRA: '',
            respHost: '',
            respHostJson: '',
            tam: 0,
            type: processType_1.ProcessType.CHIP,
        },
        remoteAddress: raddr,
        remotePort: rport,
        socket: socket,
    };
    app.locals.socketList['::ffff:'] = mpSocket;
    // The server can also receive data from the client by reading from its socket.
    socket.on('data', function (chunk) {
        var resp = Buffer.from(chunk);
        var strReply = resp.toString('hex');
        var raddr = socket.remoteAddress;
        let sock2 = app.locals.socketList['::ffff:'];
        console.log('buffer string:', strReply);
        libPOS.log('INFO', `POS: ${resp.toString('hex')} (${resp.length})`);
        sock2.options.tam += resp.length;
        //LibPos.log('INFO',`Tam:  ${tam}`);
        switch (sock2.options.type) {
            case processType_1.ProcessType.CHIP:
                (0, flowPOS_1.flujoChip)(strReply, sock2);
                break;
            case processType_1.ProcessType.CTL:
                (0, flowPOS_1.flujoCtl)(strReply, sock2);
                break;
            case processType_1.ProcessType.Inicio:
                (0, flowPOS_1.flujoInicio)(strReply, sock2);
                break;
            case processType_1.ProcessType.Cierre:
                (0, flowPOS_1.flujoCierre)(strReply, sock2);
                break;
            case processType_1.ProcessType.Anulacion:
                (0, flowPOS_1.flujoAnulacion)(strReply, sock2);
                break;
            default:
                break;
        }
    });
    // When the client requests to end the TCP connection with the server, the server
    // ends the connection.
    socket.on('close', function (data) {
        var islistening = server.listening;
        var destroyed = socket.destroyed;
        libPOS.log('WARN', 'Socket Close');
    });
    // Don't forget to catch error, for your own sake.
    socket.on('error', function (err) {
        var islistening = server.listening;
        var destroyed = socket.destroyed;
        libPOS.log('ERROR', 'Socket Error');
        //LibPos.log('ERROR','-- Servidor TCP escuchando: ' + islistening);
        //LibPos.log('ERROR','-- Socket destroyed: ' + destroyed);
        libPOS.log('ERROR', `-- Error: ${err.toString()}`);
        libPOS.log('ERROR', err.stack);
    });
    socket.on('end', function (data) {
        var islistening = server.listening;
        var destroyed = socket.destroyed;
        //clearInterval(interval);
        libPOS.log('WARN', 'Socket End');
    });
});
server.listen(tcpPort, function () {
    //'listening' listener
    libPOS.log('INFO', 'Servidor escuchando en el puerto: ' + tcpPort);
});
// emits when any error occurs -> calls closed event immediately after this.
server.on('error', function (error) {
    libPOS.log('ERROR', 'Server Error');
    libPOS.log('ERROR', '-- Error: ' + error.toString());
});
//# sourceMappingURL=app.js.map