"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProcessType = void 0;
var ProcessType;
(function (ProcessType) {
    ProcessType[ProcessType["CHIP"] = 0] = "CHIP";
    ProcessType[ProcessType["CTL"] = 1] = "CTL";
    ProcessType[ProcessType["Inicio"] = 2] = "Inicio";
    ProcessType[ProcessType["Cierre"] = 3] = "Cierre";
    ProcessType[ProcessType["Anulacion"] = 4] = "Anulacion";
})(ProcessType = exports.ProcessType || (exports.ProcessType = {}));
//# sourceMappingURL=processType.js.map