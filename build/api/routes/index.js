"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.interval = exports.posRouter = void 0;
const POSRouter_1 = __importDefault(require("./POS/POSRouter"));
exports.posRouter = POSRouter_1.default;
const POSRouter_2 = require("./POS/POSRouter");
Object.defineProperty(exports, "interval", { enumerable: true, get: function () { return POSRouter_2.interval; } });
//# sourceMappingURL=index.js.map