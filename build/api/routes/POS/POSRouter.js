"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.interval = void 0;
const express_1 = require("express");
const apiResponse_1 = require("../../../utils/apiResponse");
const asyncHandler_1 = __importDefault(require("../../../utils/asyncHandler"));
const processType_1 = require("../../model/processType");
var libPOS = require('../../Library/libPOS');
var intr;
exports.interval = intr;
var posRouter = (0, express_1.Router)();
posRouter.get('/chip/:ammount', (0, asyncHandler_1.default)(async (req, res) => {
    try {
        var posSocket = req.app.locals.socketList['::ffff:'];
        posSocket.options.type = processType_1.ProcessType.CHIP;
        posSocket.options.respHostJson = '';
        var ammount = parseFloat(req.params.ammount);
        console.log('MONTO TRANSACCION:', ammount);
        libPOS.log('INFO', 'Pago con tarjeta Chip:' + ammount);
        if (isNaN(ammount)) {
            res.send('El monto debe ser un numero entero mayor a cero');
        }
        else {
            let ammountString = ammount.toFixed(2);
            posSocket.options.montoBOB = libPOS.ValidarMonto(ammountString);
            posSocket.options.paso = 1;
            posSocket.options.tam = 0;
            //sock2.write(solConnHex); console.log(`CAJA: Enviado solConn al POS: ${solConn}`);
            libPOS.SendConnectionChip(posSocket.socket);
            exports.interval = intr = setInterval(function () {
                var lenr = posSocket.options.respHostJson.length;
                if (lenr > 1) {
                    libPOS.log('INFO', 'Resp:' + posSocket.options.respHostJson);
                    if ('error' in posSocket.options.respHostObject) {
                        new apiResponse_1.FailureMsgResponse('Ha ocurrido un error con la transacción, intente nuevamente').send(res);
                    }
                    else {
                        new apiResponse_1.SuccessResponse('Transaccion realizada correctamente', posSocket.options.respHostObject).send(res);
                    }
                    clearInterval(intr);
                    posSocket.options.respHost = '';
                }
            }, 3000);
        }
    }
    catch (e) {
        console.log('Error', e);
        new apiResponse_1.FailureMsgResponse('Error, revise que el dispositivo se encuentra conectado').send(res);
    }
}));
posRouter.get('/ctl/:ammount/', (0, asyncHandler_1.default)(async (req, res) => {
    try {
        //res.send(req.params)
        var posSocket = req.app.locals.socketList['::ffff:'];
        posSocket.options.type = processType_1.ProcessType.CTL;
        posSocket.options.respHostJson = '';
        var ammount = parseFloat(req.params.ammount);
        console.log('MONTO TRANSACCION:', ammount);
        libPOS.log('INFO', 'Pago con tarjeta CTL:' + ammount);
        if (isNaN(ammount)) {
            res.send('El monto debe ser un numero entero mayor a cero');
        }
        else {
            let ammountString = ammount.toFixed(2);
            posSocket.options.montoBOB = libPOS.ValidarMonto(ammountString);
            posSocket.options.paso = 1;
            posSocket.options.tam = 0;
            //sock2.write(solConnHex); console.log(`CAJA: Enviado solConn al POS: ${solConn}`);
            libPOS.SendConnectionCtl(posSocket.socket);
            exports.interval = intr = setInterval(function () {
                var lenr = posSocket.options.respHostJson.length;
                if (lenr > 1) {
                    libPOS.log('INFO', 'Resp:' + posSocket.options.respHostJson);
                    if ('error' in posSocket.options.respHostObject) {
                        new apiResponse_1.FailureMsgResponse('Ha ocurrido un error con la transacción, intente nuevamente').send(res);
                    }
                    else {
                        new apiResponse_1.SuccessResponse('Transaccion realizada correctamente', posSocket.options.respHostObject).send(res);
                    }
                    clearInterval(intr);
                    posSocket.options.respHost = '';
                }
            }, 3000);
        }
    }
    catch (e) {
        console.log('Error', e);
        new apiResponse_1.FailureMsgResponse('Error, revise que el dispositivo se encuentra conectado').send(res);
    }
}));
posRouter.get('/inicializar/:confirmar', (0, asyncHandler_1.default)(async (req, res) => {
    var posSocket = req.app.locals.socketList['::ffff:'];
    posSocket.options.type = processType_1.ProcessType.Inicio;
    posSocket.options.confirmation = parseInt(req.params.confirmar);
    posSocket.options.respHostJson = '';
    libPOS.log('INFO', 'Inicializacion de Pos');
    //res.send('Pago con tarjeta CTL:' + req.params.monto);
    //console.log('-- Socket: ' + rport);
    if (posSocket.options.confirmation != 1) {
        res.send('Inicializacion no autorizada');
    }
    else {
        //res.send('Monto:' + montoBOB);
        posSocket.options.paso = 1;
        posSocket.options.tam = 0;
        //sock2.write(solConnHex); console.log(`CAJA: Enviado solConn al POS: ${solConn}`);
        libPOS.SendSolicitudInicializar(posSocket.socket);
        exports.interval = intr = setInterval(function () {
            var lenr = posSocket.options.respHostJson.length;
            if (lenr > 1) {
                libPOS.log('INFO', 'Resp:' + posSocket.options.respHostJson);
                res.send(posSocket.options.respHostJson);
                clearInterval(intr);
                posSocket.options.respHostJson = '';
            }
        }, 3000);
    }
}));
posRouter.get('/cierre/:confirmar', (0, asyncHandler_1.default)(async (req, res) => {
    try {
        //res.send(req.params)
        var posSocket = req.app.locals.socketList['::ffff:'];
        posSocket.options.type = processType_1.ProcessType.Cierre;
        posSocket.options.confirmation = parseInt(req.params.confirmar);
        posSocket.options.respHostJson = '';
        libPOS.log('INFO', 'Cierre de lote');
        //res.send('Pago con tarjeta CTL:' + req.params.monto);
        //console.log('-- Socket: ' + rport);
        if (posSocket.options.confirmation != 1) {
            res.send('Cierre no autorizado');
        }
        else {
            //res.send('Monto:' + montoBOB);
            posSocket.options.paso = 1;
            posSocket.options.tam = 0;
            //sock2.write(solConnHex); console.log(`CAJA: Enviado solConn al POS: ${solConn}`);
            libPOS.SendSolicitudCierre(posSocket.socket);
            exports.interval = intr = setInterval(function () {
                var lenr = posSocket.options.respHostJson.length;
                if (lenr > 1) {
                    libPOS.log('INFO', 'Resp:' + posSocket.options.respHostJson);
                    if ('error' in posSocket.options.respHostObject) {
                        new apiResponse_1.FailureMsgResponse('Ha ocurrido un error con el cierre de cajas, intente nuevamente').send(res);
                    }
                    else {
                        new apiResponse_1.SuccessResponse('Cierre de cajas realizado correctamente', posSocket.options.respHostObject).send(res);
                    }
                    clearInterval(intr);
                    posSocket.options.respHostJson = '';
                    //libPOS = '';
                }
            }, 3000);
        }
    }
    catch (e) {
        console.log('Error', e);
        new apiResponse_1.FailureMsgResponse('Error, revise que el dispositivo se encuentra conectado').send(res);
    }
}));
posRouter.get('/anulacion/:recibo', (0, asyncHandler_1.default)(async (req, res) => {
    var posSocket = req.app.locals.socketList['::ffff:'];
    posSocket.options.type = processType_1.ProcessType.Anulacion;
    posSocket.options.confirmation = parseInt(req.params.confirmar);
    posSocket.options.respHostJson = '';
    var recibo = parseInt(req.params.recibo);
    libPOS.log('INFO', 'Anulacion de transaccion:' + recibo);
    if (isNaN(recibo)) {
        res.send('El monto debe ser un numero entero mayor a cero');
    }
    else {
        posSocket.options.reciboTRA = libPOS.ValidarRef(recibo);
        //res.send('Monto:' + montoBOB);
        posSocket.options.paso = 1;
        posSocket.options.tam = 0;
        //sock2.write(solConnHex); console.log(`CAJA: Enviado solConn al POS: ${solConn}`);
        libPOS.SendSolicitudAnulacion(posSocket.socket);
        exports.interval = intr = setInterval(function () {
            var lenr = posSocket.options.respHostJson.length;
            if (lenr > 1) {
                libPOS.log('INFO', 'Resp:' + posSocket.options.respHostJson);
                res.send(posSocket.options.respHostJson);
                clearInterval(intr);
                posSocket.options.respHost = '';
            }
        }, 3000);
    }
}));
exports.default = posRouter;
//# sourceMappingURL=POSRouter.js.map